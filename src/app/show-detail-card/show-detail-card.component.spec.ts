import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDetailCardComponent } from './show-detail-card.component';

describe('ShowDetailCardComponent', () => {
  let component: ShowDetailCardComponent;
  let fixture: ComponentFixture<ShowDetailCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowDetailCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDetailCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
