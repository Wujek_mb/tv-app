export interface Show {
  id: number,
  name: string,
  imageSmall: string,
  imageBig: string,
  language: string,
  genres: string[],
  runtime: number,
  summary: string,
  officialSite: string,
  type: string
}
