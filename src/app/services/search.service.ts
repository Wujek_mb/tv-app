import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/from';
import { Show } from '../common/show.model';

@Injectable()
export class SearchService {
  private readonly searchUrl: string = 'http://api.tvmaze.com/search/shows?q=';
  private tvshows: any;
  public tvShowsParsed = [];
  public tvShowsStreamParsed$ = new Subject();

  constructor(private http: Http) {
  }

  public search(query: string) {
    this.http.get(this.searchUrl + query)
      .subscribe((res) => {
        this.tvshows = res;
        this.tvShowsParsed = this.tvshows.map(show =>
          this.repoToModel(show.show)
        );
        this.tvShowsStreamParsed$.next(this.tvShowsParsed);
      });
  }

  public getParsedStream$(): Observable<Show> {
    return Observable.from(this.tvShowsStreamParsed$).startWith(this.tvShowsParsed);
  }

  public getShow(id: number): Show {
    let shows;
    this.getParsedStream$().subscribe(show => shows = show);
    return shows.find(show => show.id === id)
  }

  public repoToModel(show): Show {
    return {
      id: show.id,
      name: show.name,
      imageSmall: show.image ? show.image.medium : 'http://static.tvmaze.com/images/no-img/no-img-portrait-text.png',
      imageBig: show.image ? show.image.original : 'http://static.tvmaze.com/images/no-img/no-img-portrait-text.png',
      language: show.language,
      genres: show.genres[1]  ? show.genres : ['Genres', 'not', 'set'],
      runtime: show.runtime,
      summary: show.summary,
      officialSite: show.officialSite,
      type: show.type
    }
  }
}
