export interface Options {
  showAddButton: boolean,
  showEditButton: boolean,
  showBackButton: boolean,
  showRemoveButton: boolean,
  value: string
}
