import { Component, OnDestroy, OnInit } from '@angular/core';
import { Show } from '../common/show.model';
import { ActivatedRoute } from '@angular/router';
import { ListService } from '../services/list.service';
import { Options } from '../common/options.model';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'tv-list-detail',
  templateUrl: './list-detail.component.html',
  styleUrls: ['./list-detail.component.scss']
})
export class ListDetailComponent implements OnInit, OnDestroy {

  public show: Show;
  public editForm: boolean = false;
  private subscribption: Subscription;

  public options: Options = {
    showRemoveButton: true,
    showAddButton: false,
    showEditButton: true,
    showBackButton: true,
    value: 'MY-SHOWS'
  };

  constructor(private activeRoute: ActivatedRoute,
              private listService: ListService) {
  }

  public ngOnInit(): void {
    this.subscribption = this.listService.getEditStream().subscribe(eddited => this.editForm = eddited);
    this.getShow();
  }

  public ngOnDestroy(): void {
    if (this.subscribption) {
      this.subscribption.unsubscribe();
    }
  }

  private getShow(): void {
    const id: number = +this.activeRoute.snapshot.params['id'];
    this.show = this.listService.getShow(id);
  }
}
