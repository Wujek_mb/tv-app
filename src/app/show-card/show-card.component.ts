import { Component, Input } from '@angular/core';

@Component({
  selector: 'tv-show-card',
  templateUrl: './show-card.component.html',
  styleUrls: ['./show-card.component.scss']
})
export class ShowCardComponent {

  @Input() public imageUrl: string;
  @Input() public tags: string;
  @Input() public title: string;

  constructor() {
  }

  getTags() {
    return this.tags.split(',');
  }

}
