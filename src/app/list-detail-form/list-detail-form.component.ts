import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Show } from '../common/show.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ListService } from '../services/list.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'tv-list-detail-form',
  templateUrl: './list-detail-form.component.html',
  styleUrls: ['./list-detail-form.component.scss']
})
export class ListDetailFormComponent implements OnInit, OnDestroy {

  @Input()
  public show: Show;
  private subscription: Subscription;
  public showForm: FormGroup;
  public formErrors = {
    'name': '',
    'genres': '',
    'valid': '',
    'language': '',
    'type': '',
    'runtime': '',
    'imageBig': '',
    'summary': ',',
    'officialSite': ''
  };
  private validationMessages = {
    'name': {
      'required': 'Name is required.',
      'minlength': 'Name must be at least 3 characters long.',
      'maxlength': 'Name cannot be more than 24 characters long.'
    },
    'genres': {
      'minlength': 'minimum for genre is 3 letters, for example : any'
    },
    'language': {},
    'imageBig': {
      'required': 'You want to have image of the Show!'
    },
    'summary': {
      'required': 'Describe show!',
      'minlength': 'Description cannot be too short.',
      'maxlength': 'Too long!'
    },
    'officialSite': {
      'required': 'Add official site link !'
    },
    'type': {
      'minlength': 'too short, minimum example : any'
    },
    'runtime': {
      'required': 'You must specify runtime of the Show.',
      'minlength': 'Runtime cannot be 0.',
      'maxlength': 'Too long Show duration!'
    }
  };

  constructor(private fb: FormBuilder, private listService: ListService,
              private router: Router) {
  }

  ngOnInit() {
    this.createForm();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private createForm(): void {
    this.showForm = this.fb.group({
      'name': [this.show.name, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(24)
      ]],
      id: this.show.id,
      imageSmall: this.show.imageSmall,
      'language': [this.show.language],
      'genres': [this.show.genres, []],
      'imageBig': [this.show.imageBig, [
        Validators.required
      ]],
      'runtime': [this.show.runtime, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(3)
      ]],
      'summary': [this.show.summary, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(1000)
      ]],
      'type': [this.show.type, []],
      'officialSite': [this.show.officialSite]
    });

    this.subscription = this.showForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }

  private onValueChanged(data?: any) {
    if (!this.showForm) {
      return;
    }
    const form = this.showForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  public saveShow(): void {
    const show = this.showForm.value;
    if (this.showForm.hasError) {
      this.formErrors.valid = ' ';
      this.listService.editShow(show).subscribe(() => {
        this.router.navigate(['/mylist']);
      });
    } else {
      this.formErrors.valid = 'Correct mistakes!!!';
    }
  }

}
