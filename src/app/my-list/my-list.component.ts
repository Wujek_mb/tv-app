import { Component, OnInit } from '@angular/core';
import { ListService } from '../services/list.service';
import { Show } from '../common/show.model';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'tv-my-list',
  templateUrl: './my-list.component.html',
  styleUrls: ['./my-list.component.scss'],
})
export class MyListComponent implements OnInit {

  public removed: boolean = this.listService.removed;
  public edited: boolean = this.listService.edited;
  public shows: Observable<Show>;
  public page: number = 1;

  constructor(private listService: ListService) {
  }

  ngOnInit() {
    this.getShows();
  }

  private getShows() {
    this.listService.getShows();
    this.shows = this.listService.getShowStream();
  }

}
