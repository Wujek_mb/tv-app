import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SearchService } from '../services/search.service';
import { ListService } from '../services/list.service';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'tv-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  public searchForm: FormGroup;
  public added: boolean = this.listService.added;
  private subscription: Subscription;

  constructor(private fb: FormBuilder,
              private searchService: SearchService,
              private listService: ListService) {
  }

  public ngOnInit(): void {
    this.createForm();
    this.subscription = this.searchForm.get('query').valueChanges
      .filter(query => query.length >= 3)
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(query => {
        this.searchService.search(query);
      });
  }

  public ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private createForm(): void {
    this.searchForm = this.fb.group({
      query: '',
    });
  }
}
