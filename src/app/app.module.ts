import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ReactiveFormsModule } from '@angular/forms';
import { RoutingModule } from './routing/routing.module';
import { AlertModule } from 'ngx-bootstrap/alert';

import { HttpCacheModule } from 'ng-http-cache';
import { NgxPaginationModule } from 'ngx-pagination';
import { OrderModule } from 'ngx-order-pipe';
import { TruncateModule } from 'ng2-truncate';


import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { ShowsListComponent } from './shows-list/shows-list.component';
import { ShowDetailComponent } from './show-detail/show-detail.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SearchService } from './services/search.service';
import { MyListComponent } from './my-list/my-list.component';
import { ListService } from './services/list.service';
import { ListDetailComponent } from './list-detail/list-detail.component';
import { ListDetailFormComponent } from './list-detail-form/list-detail-form.component';

import { ShowCardComponent } from './show-card/show-card.component';
import { ShowDetailCardComponent } from './show-detail-card/show-detail-card.component';


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    ShowsListComponent,
    ShowDetailComponent,
    NavbarComponent,
    MyListComponent,
    ListDetailComponent,
    ListDetailFormComponent,
    ShowCardComponent,
    ShowDetailCardComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RoutingModule,
    HttpCacheModule,
    NgxPaginationModule,
    OrderModule,
    TruncateModule,
    AlertModule.forRoot(),
    CollapseModule.forRoot(),
  ],
  providers: [SearchService, ListService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
