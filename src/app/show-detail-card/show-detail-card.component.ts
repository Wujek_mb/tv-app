import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Show } from '../common/show.model';
import { ListService } from '../services/list.service';
import { Location } from '@angular/common';
import { Options } from '../common/options.model';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'tv-show-detail-card',
  templateUrl: './show-detail-card.component.html',
  styleUrls: ['./show-detail-card.component.scss']
})
export class ShowDetailCardComponent implements OnDestroy {
  @Input()
  public show: Show;
  public edit: boolean = false;
  private subscriptionToAddToMyList: Subscription;
  private subscriptionToRemoveFromList: Subscription;

  @Input()
  public options: Options;

  constructor(private listService: ListService,
              private location: Location) {
  }

  ngOnDestroy(): void {
    if (this.subscriptionToAddToMyList) {
      this.subscriptionToAddToMyList.unsubscribe();
    }
    if (this.subscriptionToRemoveFromList) {
      this.subscriptionToRemoveFromList.unsubscribe();
    }
  }

  public addToMyList(show: Show): void {
    this.subscriptionToAddToMyList = this.listService.addToMyList(show).subscribe();
    this.goBack();
  }

  public goBack(): void {
    this.location.back();
  }

  public editShow(): void {
    this.edit = this.edit === false;
    this.listService.showForm(this.edit);
  }

  public getGenres(): string[] {
    return this.show.genres.toString().split(',');
  }

  public removeShow(id: number): void {
    this.subscriptionToRemoveFromList = this.listService.removeFromList(id).subscribe();
    this.goBack();
  }

}
