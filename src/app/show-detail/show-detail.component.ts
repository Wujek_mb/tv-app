import { Component, OnInit } from '@angular/core';
import { SearchService } from '../services/search.service';
import { ActivatedRoute } from '@angular/router';
import { Show } from '../common/show.model';
import { Options } from '../common/options.model';

@Component({
  selector: 'tv-show-detail',
  templateUrl: './show-detail.component.html',
  styleUrls: ['./show-detail.component.scss']
})
export class ShowDetailComponent implements OnInit {
  public show: Show;

  public options: Options = {
    showAddButton: true,
    showBackButton: true,
    showEditButton: false,
    showRemoveButton: false,
    value: 'SEARCH'
  };

  constructor(private searchService: SearchService,
              private activeRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.getShow();
  }

  private getShow(): void {
    const id: number = +this.activeRoute.snapshot.params['id'];
    this.show = this.searchService.getShow(id);
  }
}
