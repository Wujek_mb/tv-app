import { Component, OnInit, OnDestroy } from '@angular/core';
import { SearchService } from '../services/search.service'
import { Show } from '../common/show.model';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'tv-shows-list',
  templateUrl: './shows-list.component.html',
  styleUrls: ['./shows-list.component.scss']
})
export class ShowsListComponent implements OnInit, OnDestroy {
  public shows: Show;
  subscribeToGetParsed: Subscription;
  public page: number = 1;
  public value: string = '';
  public orderBy = [
    {value: 'name', display: 'name '},
    {value: 'runtime', display: 'runtime '}
  ];
  public reverse = {value: false, display: '▲'};

  constructor(private searchService: SearchService) {
  }

  public ngOnInit(): void {
    this.subscribeToGetParsed = this.searchService
      .getParsedStream$()
      .subscribe(shows => this.shows = shows);
  }

  public ngOnDestroy(): void {
    if (this.subscribeToGetParsed) {
      this.subscribeToGetParsed.unsubscribe();
    }
  }

  public setReverseOrder(): void {
    if (this.reverse.value === false) {
      this.reverse.value = true;
      this.reverse.display = '▼';
    } else if (this.reverse.value === true) {
      this.reverse.value = false;
      this.reverse.display = '▲';
    }
  }
}
