import { TvAppPage } from './app.po';

describe('tv-app App', () => {
  let page: TvAppPage;

  beforeEach(() => {
    page = new TvAppPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to tv!!'))
      .then(done, done.fail);
  });
});
