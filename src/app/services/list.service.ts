import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Show } from '../common/show.model';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ListService {
  public showsStream$: Subject<Show> = new Subject();
  public shows;
  public editStream$: Subject<boolean> = new Subject();
  public readonly url = 'http://localhost:3000/show/';
  public added: boolean = false;
  public removed: boolean = false;
  public edited: boolean = false;

  constructor(private http: Http) {
  }

  public addToMyList(show: Show): Observable<Response> {
    this.added = true;
    setTimeout(() => {
      this.added = false
    }, 5000);
    return this.http.post(this.url, show);
  }

  public showForm(editted: boolean) {
    this.editStream$.next(editted);
  }

  public getEditStream(): Observable<boolean> {
    const choice = false;
    return Observable.from(this.editStream$).startWith(choice);
  }

  public editShow(show: Show): Observable<Response> {
    this.edited = true;
    setTimeout(() => {
      this.edited = false
    }, 5000);
    if (show.id) {
      return this.http.put(this.url + show.id, show);
    }
  }

  public getShows(): void {
    this.http.get(this.url).subscribe(show => {
      this.shows = show;
      this.showsStream$.next(this.shows);
    });
  }

  public getShowStream(): Observable<Show> {
    return Observable.from(this.showsStream$).startWith(this.shows);
  }

  public getShow(id: number): Show {
    let shows;
    this.getShowStream().subscribe(show => shows = show);
    return shows.find(show => show.id === id);
  }

  public removeFromList(id: number): Observable<Response> {
    this.removed = true;
    setTimeout(() => {
      this.removed = false
    }, 5000);
    return this.http.delete(this.url + id);
  }
}
