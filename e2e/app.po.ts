import { browser, by, element } from 'protractor';

export class TvAppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('tv-root h1')).getText();
  }
}
