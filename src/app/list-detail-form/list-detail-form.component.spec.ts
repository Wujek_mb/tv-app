import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDetailFormComponent } from './list-detail-form.component';

describe('ListDetailFormComponent', () => {
  let component: ListDetailFormComponent;
  let fixture: ComponentFixture<ListDetailFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDetailFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDetailFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
