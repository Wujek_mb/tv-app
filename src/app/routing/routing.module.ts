import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { ShowDetailComponent } from '../show-detail/show-detail.component';
import { MyListComponent } from '../my-list/my-list.component';
import { SearchComponent } from '../search/search.component';
import { ListDetailComponent } from '../list-detail/list-detail.component';

const routes: Routes = [
  {path: '', redirectTo: '/shows', pathMatch: 'full'},
  {path: 'shows', component: SearchComponent},
  {path: 'shows/show/:id', component: ShowDetailComponent},

  {path: 'mylist', component: MyListComponent},
  {path: 'mylist/show/:id', component: ListDetailComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class RoutingModule {
}
